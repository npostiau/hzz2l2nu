# Data files

This directory contains auxiliary files with various corrections. They are organized into several subdirectories:


## JERC

* `Summer16_07Aug2017*_V11_*_AK4PFchs.txt` <br />
  Files defining JEC and its total uncertainty for 2016. Downloaded from the JERC repository by running `./download_jec.py 2016`. Recommended [here](https://twiki.cern.ch/twiki/bin/viewauth/CMS/JECDataMC?rev=166#Jet_Energy_Corrections_in_Run2).
* `Summer16_25nsV1_MC_PtResolution_AK4PFchs.txt` <br />
  Jet p<sub>T</sub> resolution in `Summer16` simulation. File copied from [here](https://github.com/cms-jet/JRDatabase/blob/master/textFiles/Summer16_25nsV1_MC/Summer16_25nsV1_MC_PtResolution_AK4PFchs.txt).
* `Summer16_25nsV1_MC_SF_AK4PFchs.txt` <br />
  Data-to-simulation scale factors for jet p<sub>T</sub> resolution for 2016. File copied from [here](https://github.com/cms-jet/JRDatabase/blob/master/textFiles/Summer16_25nsV1_MC/Summer16_25nsV1_MC_SF_AK4PFchs.txt).


## LeptonSF

* `2016Muon_SF_ID.root` and `2016Muon_SF_ISO.root`  <br />
  2016 Muon SFs copied from Muon POG [here](https://twiki.cern.ch/twiki/bin/view/CMS/MuonReferenceEffs2016LegacyRereco?rev=10).
* `2017Muon_SF_ID.root` and `2017Muon_SF_ISO.root`  <br />
  2017 Muon SFs copied from Muon POG [here](https://twiki.cern.ch/twiki/bin/view/CMS/MuonReferenceEffs2017?rev=30).
* `2018Muon_SF_ID.root` and `2018Muon_SF_ISO.root`  <br />
  2018 Muon SFs copied from Muon POG [here](https://twiki.cern.ch/twiki/bin/view/CMS/MuonReferenceEffs2018?rev=8).
* `201*Electron_SF_IDISO.root` and `201*Electron_SF_RECO.root`  <br />
  Electron scale factors for all years copied from EGamma POG [here](https://twiki.cern.ch/twiki/bin/view/CMS/EgammaRunIIRecommendations?rev=15#Electron_Scale_Factors).
  There are several sets of electron scale factors. 
  For 2016 RECO SFs, we use `legacy`, `ET > 20GeV`.
  For ID&ISO SFs, we use `Fall17v2` `cutBasedElectronID-Fall17-94X-V2-tight` [here](https://twiki.cern.ch/twiki/bin/view/CMS/EgammaRunIIRecommendations?rev=15#Fall17v2)

